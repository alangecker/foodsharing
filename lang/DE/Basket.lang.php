<?php
global $g_lang;
$g_lang['picture-desc'] = 'Füge dem Essenskorb ein Foto hinzu!';
$g_lang['picture-choose'] = 'Datei wählen';
$g_lang['description'] = 'Beschreibung';
$g_lang['weight'] = 'Geschätztes Gewicht';
$g_lang['food_type'] = 'Welche Arten von Lebensmitteln sind dabei?';
$g_lang['food_art'] = 'Was trifft auf die Lebensmittel zu?';
$g_lang['contactmessage'] = 'Per Nachricht';
$g_lang['fetchstate'] = 'Hat alles gut geklappt?';
$g_lang['contact_type'] = 'Wie möchtest Du kontaktiert werden?';
$g_lang['tel'] = 'Festnetztelefon';
$g_lang['lifetime'] = 'Wie lange soll Dein Essenskorb gültig sein?';
$g_lang['lifetime_options'] = ['einen Tag', 'zwei Tage', 'drei Tage', 'eine Woche', 'zwei Wochen', 'drei Wochen'];

$g_lang['set_date'] = 'Einstelldatum';
$g_lang['phone_contact'] = 'Telefonisch kontaktieren';
$g_lang['foodsaver_contact'] = '{name} kontaktieren';
$g_lang['provider'] = 'AnbieterIn';
$g_lang['create_at'] = 'Veröffentlicht am';
$g_lang['until'] = 'Gültig bis';
$g_lang['update_at'] = 'Geändert am';
$g_lang['basket_near'] = 'In Deiner Nähe';

$g_lang['contact_info'] = 'Kontakt';
$g_lang['open_chat'] = 'Nachricht schreiben';

$g_lang['no_requests'] = 'Bisher keine Anfragen';
$g_lang['n_requests'] = 'Bisherige Anfragen: <strong>{count}</strong>';
$g_lang['basket_requested_by'] = 'angefragt von';
$g_lang['req_count'] = 'Anfragen {count}';
$g_lang['send_request'] = 'Anfrage absenden';
$g_lang['sent_request'] = 'Anfrage wurde versendet';
$g_lang['withdraw_request'] = 'Anfrage zurückziehen';
$g_lang['withdrawed_request'] = 'Anfrage wurde zurückgezogen';
$g_lang['finish_request'] = 'Danke Dir! Der Vorgang ist abgeschlossen.';

$g_lang['request_empty_message_error'] = 'Dein Anfragetext darf nicht leer sein.';
$g_lang['request_denied_error'] = 'Deine Anfrage wurde vom Essenskorbanbieter abgelehnt.';
$g_lang['request_basket_404_error'] = 'Der Essenskorb existiert nicht mehr.';

$g_lang['baskets'] = 'Essenskörbe';
$g_lang['basket'] = 'Essenskorb';
$g_lang['basket_offer'] = 'Essenskorb anbieten';
$g_lang['basket_edit'] = 'Essenskorb bearbeiten';
$g_lang['basket_reference'] = 'Hinweis!';
$g_lang['basket_reference_info'] = 'Beschreibung, Bild, Übergabeort und Zeitraum sind öffentlich sichtbar.';
$g_lang['basket_request'] = 'Essenskorb anfragen';
$g_lang['basket_request_on_page'] = 'Essenskorb anfragen auf unserer foodsharing-Homepage';
$g_lang['basket_pickup_warning'] = '<b>Information zur Benutzung von Essenskörben</b> <br>Bitte nutze die grünen Schaltflächen <b>"Essenskorb anfragen"</b>, um Interesse an diesem Korb zu bekunden, oder <b>"Essenskorb bearbeiten"</b>, um Deinen eigenen Korb nachträglich zu bearbeiten!';
$g_lang['basket_delete'] = 'Essenskorb löschen';
$g_lang['basket_publish'] = 'Essenskorb veröffentlichen';
$g_lang['to_basket'] = 'zum Essenskorb';
$g_lang['basket_foodsaver'] = 'Essenskorb von {name}';
$g_lang['basket_foodsaver_close'] = 'Essenskorbanfrage von {name} abschließen';
$g_lang['basket_request_close'] = 'Essenskorbanfrage abschließen';
$g_lang['basket_error'] = 'Essenskorb konnte nicht geladen werden.';
$g_lang['basket_error_message'] = 'Du hast keine Nachricht eingegeben';
$g_lang['basket_publish_error'] = 'Es gab einen Fehler. Der Essenskorb konnte nicht veröffentlicht werden.';
$g_lang['basket_publish_error_desc'] = 'Bitte gib eine Beschreibung ein!';
$g_lang['basket_publish_error_permission'] = 'Sie haben keine Rechte den Essenskorb zu bearbeiten.';
$g_lang['basket_publish_thank_you'] = 'Danke Dir! Der Essenskorb wurde veröffentlicht!';
$g_lang['basket_publish_error_address'] = '<br>Bitte gib Deine Adresse in den <a href="/?page=settings&sub=general">Einstellungen</a> an, um einen Essenskorb über die Webseite anbieten zu können.<br><br>Diese wird benötigt, um einen Essenskorb mittels Pinnadel auf der Karte darzustellen.<br><br>Die Postanschrift Deiner Adresse ist dabei für EssenskorbnutzerInnen nicht sichtbar.';
$g_lang['go_to_settings'] = 'Zu den Einstellungen';

$g_lang['basket_deleted_picked_up'] = 'Ja, {gender} hat den Korb abgeholt.';
$g_lang['basket_not_picked_up'] = 'Nein, {gender} ist leider nicht wie verabredet erschienen.';
$g_lang['basket_deleted_other_reason'] = 'Die Lebensmittel wurden von jemand anderem abgeholt.';
$g_lang['basket_request_deny'] = 'Ich möchte diese Anfrage ablehnen.';
$g_lang['basket_not_active'] = 'Essenskorb ist jetzt nicht mehr aktiv!';
$g_lang['basket_picked_up'] = '<b>Dieser Essenskorb ist leider nicht mehr verfügbar!</b> <br><br> Bist Du noch nicht als Foodsharer registriert? Dann klick oben auf <b>Mach mit!</b> um loszulegen oder stelle selbst einen Essenskorb ein!<br><br>Auf unserer Karte findest Du gegebenenfalls weitere Essenksörbe in Deiner Gegend.';

$g_lang['basket_detail_login_hint'] = 'Für detaillierte Infos musst Du eingeloggt sein. Bist Du noch nicht als Foodsharer registriert? Dann klick oben auf <b>Mach mit!</b> um loszulegen!';
$g_lang['basket_on_map'] = 'Alle Körbe auf der Karte';
$g_lang['not_login_hint'] = 'Du bist nicht eingeloggt, vielleicht ist Deine Session abgelaufen, bitte logge Dich ein und sende Deine Anfrage erneut ab.';
