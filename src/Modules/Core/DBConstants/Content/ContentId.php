<?php

namespace Foodsharing\Modules\Core\DBConstants\Content;

/*
 *
 * At the beginning of this class there were up to 49 content ids given as number
 * please replace them by constants to make them easier to read.
 *
 * You can find them by searching for the use of `$this->contentGateway->get()`
 * */

class ContentId
{
	public const PARTNER_PAGE_10 = 10;
	public const SUPPORT_FOODSHARING_PAGE_42 = 42;
}
